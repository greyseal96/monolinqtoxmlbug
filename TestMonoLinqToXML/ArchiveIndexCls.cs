using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace TestMonoLinqToXML
{
    public class ArchiveCls
    {
        public string ArchiveID { get; set; }
        public string ArchiveFile { get; set; }
        public string ArchiveDesc { get; set; }
    }

    public class ContainerCls
    {
        public string ContainerName { get; set; }
        public List<ArchiveCls> ArchivesList { get; set; }

        public ContainerCls(string containerName)
        {
            this.ContainerName = containerName;
            this.ArchivesList = new List<ArchiveCls>();
        }
    }

    public class AreaCls
    {
        public string AreaName { get; set; }
        public List<ContainerCls> ContainersList { get; set; }

        public AreaCls(string areaName)
        {
            this.AreaName = areaName;
            this.ContainersList = new List<ContainerCls>();
        }
    }

    public class BackupAcctCls
    {
        public string BackupAcctID { get; set; }
        public List<AreaCls> AreasList { get; set; }

        public BackupAcctCls(string backupAcctID)
        {
            this.BackupAcctID = backupAcctID;
            this.AreasList = new List<AreaCls>();
        }
    }
    public class ArchiveIndexCls
    {

        #region Fields
        private string indexFilePath;
        private List<BackupAcctCls> backupAccts;
        #endregion

        #region Properties

        #endregion

        #region Constructors
        public ArchiveIndexCls(string indexFilePath)
        {
            this.indexFilePath = indexFilePath;
            backupAccts = new List<BackupAcctCls>();
        }
        #endregion

        #region Methods
        private void populateStructures()
        {
            try
            {
                //Load the document
                XDocument archiveIndexFile = XDocument.Load(indexFilePath);

                //There will will potentially be multiple backup accounts stored.  By picking
                //the xdocuments descendants that are named "BackupAccount", we will get all
                //of the backup accounts stored in the xdocument.
                //This is the first thing that I tried:  loading up everything from the document
                //in one big foreach loop.
                foreach (XElement backupAcctElement in archiveIndexFile.Descendants("BackupAccount"))
                {
                    BackupAcctCls backupAcct = new BackupAcctCls(backupAcctElement.Attribute("AcctID").Value);
                    backupAccts.Add(backupAcct);

                    foreach (XElement areaElement in backupAcctElement.Descendants("Area"))
                    {
                        AreaCls area = new AreaCls(areaElement.Attribute("Area").Value);
                        backupAcct.AreasList.Add(area);

                        foreach (XElement containerElement in areaElement.Descendants("Container"))
                        {
                            ContainerCls container = new ContainerCls(containerElement.Attribute("ContainerName").Value);
                            area.ContainersList.Add(container);

                            foreach (XElement archiveElement in containerElement.Descendants("Archive"))
                            {
                                ArchiveCls archive = new ArchiveCls();
                                archive.ArchiveID = archiveElement.Attribute("ArchiveID").Value;
                                archive.ArchiveDesc = archiveElement.Element("ArchiveDesc").Value;
                                archive.ArchiveFile = archiveElement.Element("ArchiveFile").Value;
                                container.ArchivesList.Add(archive);
                            }
                        }
                    }
                }

                //Test
                this.indexFilePath += ".new";
                this.backupAccts.Find(ba => ba.BackupAcctID == "a123456").BackupAcctID = "a123456_edited";
                createNewArchiveFile();
                //End Test

                //I've tried different methods to read the file and get around the SIGSEGV error but all
                //of the different methods yield the same result.
                //My first method was to get a collection of XElements and iterate over those.  I thought
                //that I'd just test with a small collection first, rather than trying to do the whole
                //thing as I was doing above.  This had the same problem.
//                IEnumerable<XElement> backupAcctsList = archiveIndexFile.Descendants("BackupAccount");
//
//                foreach(XElement buAcctElmnt in backupAcctsList)
//                {
//                    string test = buAcctElmnt.Attribute("AcctID").Value;
//                    BackupAcctCls backupAcct = new BackupAcctCls(test);
//                    backupAccts.Add(backupAcct);
//                }

                //My second method was to try a using statement, just in case the garbage collector was
                //prematurely trying to clean up the xdocument reference.  Still no luck.
//                using(System.Xml.XmlReader xreader = System.Xml.XmlReader.Create(this.indexFilePath))
//                {
//                    XDocument archiveIndexFile = XDocument.Load(xreader, LoadOptions.None);
//                    var backupAccts = from ba in archiveIndexFile.Descendants("BackupAccount")
//                                       select new BackupAcctCls(ba.Attribute("AcctID").Value);
//
//                    foreach(BackupAcctCls baCls in backupAccts)
//                    {
//                        string test = baCls.BUAcctID;
//                    }
//                }
            }

            catch(Exception genEx)
            {
                string errMsg = genEx.Message;

            }
        }

        private void createNewArchiveFile()
        {
            //Using Linq to XML to create an XML document works just fine.
            XDocument archiveIndexFile = new XDocument(
                new XElement("BackupAccounts",
                             from ba in backupAccts
                             select new XElement("BackupAccount", new XAttribute("AcctID", ba.BackupAcctID),
                                    new XElement("Areas",
                                        from area in ba.AreasList
                                        select new XElement("Area", new XAttribute("Area", area.AreaName),
                                               new XElement("Containers",
                                                   from cntnr in area.ContainersList
                                                   select new XElement("Container", new XAttribute("ContainerName", cntnr.ContainerName),
                                                          new XElement("Archives",
                                                              from arc in cntnr.ArchivesList
                                                              select new XElement("Archive", new XAttribute("ArchiveID", arc.ArchiveID),
                                                                     new XElement("ArchiveDesc", arc.ArchiveDesc),
                                                                     new XElement("ArchiveFile", arc.ArchiveFile)
                                                                     )
                                                              )
                                                          )
                                                   )
                                               )
                                        )
                                    )
                            )
                );

            archiveIndexFile.Save(this.indexFilePath);
        }

        //Add archives to the inventory
        public void AddArchiveToInventory(string archiveFile, string archiveDesc, string archiveID)
        {
            throw new NotImplementedException();
        }

        //Display all archives in the inventory
        public string ShowAllArchives()
        {
            throw new NotImplementedException();
        }

        //Search for archive
        public string SearchForArchive(string searchString)
        {
            throw new NotImplementedException();
        }

        public void TestArchiveIndex()
        {
            //This will set up some test elements to be written to a test document.
            //Backup Accts
            BackupAcctCls buacAcct1 = new BackupAcctCls("a123456");
            BackupAcctCls buacAcct2 = new BackupAcctCls("b123456");

            //Areas
            AreaCls testArea1_ba1 = new AreaCls("westcoast-1");
            AreaCls testArea2_ba1 = new AreaCls("westcoast-2");
            AreaCls testArea1_ba2 = new AreaCls("westcoast-1");
            AreaCls testArea2_ba2 = new AreaCls("westcoast-2");

            //Containers
            ContainerCls testContainer1_a1_ba1 = new ContainerCls("MyBa1Area1TestContainer1");
            ContainerCls testContainer2_a1_ba1 = new ContainerCls("MyBa1Area1TestContainer2");
            ContainerCls testContainer1_a2_ba1 = new ContainerCls("MyBa1Area2TestContainer1");
            ContainerCls testContainer2_a2_ba1 = new ContainerCls("MyBa1Area2TestContainer2");
            ContainerCls testContainer1_a1_ba2 = new ContainerCls("MyBa2Area1TestContainer1");
            ContainerCls testContainer2_a1_ba2 = new ContainerCls("MyBa2Area1TestContainer2");
            ContainerCls testContainer1_a2_ba2 = new ContainerCls("MyBa2Area2TestContainer1");
            ContainerCls testContainer2_a2_ba2 = new ContainerCls("MyBa2Area2TestContainer2");

            //Archives
            ArchiveCls testArchive1_c1_a1_ba1 = new ArchiveCls();
            testArchive1_c1_a1_ba1.ArchiveDesc = "Test Archive File Description";
            testArchive1_c1_a1_ba1.ArchiveID = "123456789_1_c1a1ga1";
            testArchive1_c1_a1_ba1.ArchiveFile = "testArchiveFile_1_c1a1ba1.fil";
            ArchiveCls testArchive2_c1_a1_ba1 = new ArchiveCls();
            testArchive2_c1_a1_ba1.ArchiveDesc = "Test Archive File Description";
            testArchive2_c1_a1_ba1.ArchiveID = "123456789_2_c1a1ba1";
            testArchive2_c1_a1_ba1.ArchiveFile = "testArchiveFile_2_c1a1ba1.fil";
            ArchiveCls testArchive1_c2_a1_ba1 = new ArchiveCls();
            testArchive1_c2_a1_ba1.ArchiveDesc = "Test Archive File Description";
            testArchive1_c2_a1_ba1.ArchiveID = "123456789_1_c2a1ba1";
            testArchive1_c2_a1_ba1.ArchiveFile = "testArchiveFile_1_c2a1ba1.fil";
            ArchiveCls testArchive2_c2_a1_ba1 = new ArchiveCls();
            testArchive2_c2_a1_ba1.ArchiveDesc = "Test Archive File Description";
            testArchive2_c2_a1_ba1.ArchiveID = "123456789_2_c2a1ba1";
            testArchive2_c2_a1_ba1.ArchiveFile = "testArchiveFile_2_c2a1ba1.fil";
            ArchiveCls testArchive1_c1_a2_ba1 = new ArchiveCls();
            testArchive1_c1_a2_ba1.ArchiveDesc = "Test Archive File Description";
            testArchive1_c1_a2_ba1.ArchiveID = "123456789_1_c1a2ba1";
            testArchive1_c1_a2_ba1.ArchiveFile = "testArchiveFile_1_c1a2ba1.fil";
            ArchiveCls testArchive2_c1_a2_ba1 = new ArchiveCls();
            testArchive2_c1_a2_ba1.ArchiveDesc = "Test Archive File Description";
            testArchive2_c1_a2_ba1.ArchiveID = "123456789_2_c1a2ba1";
            testArchive2_c1_a2_ba1.ArchiveFile = "testArchiveFile_2_c1a2ba1.fil";
            ArchiveCls testArchive1_c2_a2_ba1 = new ArchiveCls();
            testArchive1_c2_a2_ba1.ArchiveDesc = "Test Archive File Description";
            testArchive1_c2_a2_ba1.ArchiveID = "123456789_1_c2a2ba1";
            testArchive1_c2_a2_ba1.ArchiveFile = "testArchiveFile_1_c2a2ba1.fil";
            ArchiveCls testArchive2_c2_a2_ba1 = new ArchiveCls();
            testArchive2_c2_a2_ba1.ArchiveDesc = "Test Archive File Description";
            testArchive2_c2_a2_ba1.ArchiveID = "123456789_2_c2a2ba1";
            testArchive2_c2_a2_ba1.ArchiveFile = "testArchiveFile_2_c2a2ba1.fil";
            ArchiveCls testArchive1_c1_a1_ba2 = new ArchiveCls();
            testArchive1_c1_a1_ba2.ArchiveDesc = "Test Archive File Description";
            testArchive1_c1_a1_ba2.ArchiveID = "123456789_1_c1a1ba2";
            testArchive1_c1_a1_ba2.ArchiveFile = "testArchiveFile_1_c1a1ba2.fil";
            ArchiveCls testArchive2_c1_a1_ba2 = new ArchiveCls();
            testArchive2_c1_a1_ba2.ArchiveDesc = "Test Archive File Description";
            testArchive2_c1_a1_ba2.ArchiveID = "123456789_2_c1a1ba2";
            testArchive2_c1_a1_ba2.ArchiveFile = "testArchiveFile_2_c1a1ba2.fil";
            ArchiveCls testArchive1_c2_a1_ba2 = new ArchiveCls();
            testArchive1_c2_a1_ba2.ArchiveDesc = "Test Archive File Description";
            testArchive1_c2_a1_ba2.ArchiveID = "123456789_1_c2a1ba2";
            testArchive1_c2_a1_ba2.ArchiveFile = "testArchiveFile_1_c2a1ba2.fil";
            ArchiveCls testArchive2_c2_a1_ba2 = new ArchiveCls();
            testArchive2_c2_a1_ba2.ArchiveDesc = "Test Archive File Description";
            testArchive2_c2_a1_ba2.ArchiveID = "123456789_2_c2a1ba2";
            testArchive2_c2_a1_ba2.ArchiveFile = "testArchiveFile_2_c2a1ba2.fil";
            ArchiveCls testArchive1_c1_a2_ba2 = new ArchiveCls();
            testArchive1_c1_a2_ba2.ArchiveDesc = "Test Archive File Description";
            testArchive1_c1_a2_ba2.ArchiveID = "123456789_1_c1a2ba2";
            testArchive1_c1_a2_ba2.ArchiveFile = "testArchiveFile_1_c1a2ba2.fil";
            ArchiveCls testArchive2_c1_a2_ba2 = new ArchiveCls();
            testArchive2_c1_a2_ba2.ArchiveDesc = "Test Archive File Description";
            testArchive2_c1_a2_ba2.ArchiveID = "123456789_2_c1a2ba2";
            testArchive2_c1_a2_ba2.ArchiveFile = "testArchiveFile_2_c1a2ba2.fil";
            ArchiveCls testArchive1_c2_a2_ba2 = new ArchiveCls();
            testArchive1_c2_a2_ba2.ArchiveDesc = "Test Archive File Description";
            testArchive1_c2_a2_ba2.ArchiveID = "123456789_1_c2a2ba2";
            testArchive1_c2_a2_ba2.ArchiveFile = "testArchiveFile_1_c2a2ba2.fil";
            ArchiveCls testArchive2_c2_a2_ba2 = new ArchiveCls();
            testArchive2_c2_a2_ba2.ArchiveDesc = "Test Archive File Description";
            testArchive2_c2_a2_ba2.ArchiveID = "123456789_2_c2a2ba2";
            testArchive2_c2_a2_ba2.ArchiveFile = "testArchiveFile_2_c2a2ba2.fil";

            //Add archives to containers
            testContainer1_a1_ba1.ArchivesList.Add(testArchive1_c1_a1_ba1);
            testContainer1_a1_ba1.ArchivesList.Add(testArchive2_c1_a1_ba1);

            testContainer2_a1_ba1.ArchivesList.Add(testArchive1_c2_a1_ba1);
            testContainer2_a1_ba1.ArchivesList.Add(testArchive2_c2_a1_ba1);

            testContainer1_a2_ba1.ArchivesList.Add(testArchive1_c1_a2_ba1);
            testContainer1_a2_ba1.ArchivesList.Add(testArchive2_c1_a2_ba1);

            testContainer2_a2_ba1.ArchivesList.Add(testArchive1_c2_a2_ba1);
            testContainer2_a2_ba1.ArchivesList.Add(testArchive2_c2_a2_ba1);

            testContainer1_a1_ba2.ArchivesList.Add(testArchive1_c1_a1_ba2);
            testContainer1_a1_ba2.ArchivesList.Add(testArchive2_c1_a1_ba2);

            testContainer2_a1_ba2.ArchivesList.Add(testArchive1_c2_a1_ba2);
            testContainer2_a1_ba2.ArchivesList.Add(testArchive2_c2_a1_ba2);

            testContainer1_a2_ba2.ArchivesList.Add(testArchive1_c1_a2_ba2);
            testContainer1_a2_ba2.ArchivesList.Add(testArchive2_c1_a2_ba2);

            testContainer2_a2_ba2.ArchivesList.Add(testArchive1_c2_a2_ba2);
            testContainer2_a2_ba2.ArchivesList.Add(testArchive2_c2_a2_ba2);

            //Add Containers to Areas
            testArea1_ba1.ContainersList.Add(testContainer1_a1_ba1);
            testArea1_ba1.ContainersList.Add(testContainer2_a1_ba1);

            testArea2_ba1.ContainersList.Add(testContainer1_a2_ba1);
            testArea2_ba1.ContainersList.Add(testContainer2_a2_ba1);

            testArea1_ba2.ContainersList.Add(testContainer1_a1_ba2);
            testArea1_ba2.ContainersList.Add(testContainer2_a1_ba2);

            testArea2_ba2.ContainersList.Add(testContainer1_a2_ba2);
            testArea2_ba2.ContainersList.Add(testContainer2_a2_ba2);

            //Add Areas to Backup Accounts
            buacAcct1.AreasList.Add(testArea1_ba1);
            buacAcct1.AreasList.Add(testArea2_ba1);

            buacAcct2.AreasList.Add(testArea1_ba2);
            buacAcct2.AreasList.Add(testArea2_ba2);

            this.backupAccts.Add(buacAcct1);
            this.backupAccts.Add(buacAcct2);
            //this.indexFilePath = "TestArchiveIndex.idx";

            createNewArchiveFile();
        }

        public void TestReadArchiveIndex()
        {
            populateStructures();
        }
        #endregion
    }
}

