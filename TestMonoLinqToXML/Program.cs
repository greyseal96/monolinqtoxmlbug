using System;

namespace TestMonoLinqToXML
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            string appDir = AppDomain.CurrentDomain.BaseDirectory;
            ArchiveIndexCls arcIdxCls = new ArchiveIndexCls(appDir + "TestArchiveIndex.idx");
            //Using Linq to XML to create the an xml file works fine.  Uncomment this to see
            //the file get created.
            arcIdxCls.TestArchiveIndex();

            //Linq to XML seems to always bomb out with a SIGSEGV error when trying to read the file...
            ArchiveIndexCls arcIdxCls2 = new ArchiveIndexCls(appDir + "TestArchiveIndex.idx");
            arcIdxCls2.TestReadArchiveIndex();
        }
    }
}
